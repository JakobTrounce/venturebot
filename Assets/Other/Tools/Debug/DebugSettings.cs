﻿using System;
using System.Collections;

using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class DebugLevelLoader
{
    public string levelName;
    public KeyCode loadLevelKey;

    public IEnumerator LoadSceneAsyncRoutine()
    {
        if (!string.IsNullOrEmpty(levelName))
        {
            yield return SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);
        }
        else
        {
            Debug.LogError("Error loading debug level: Level name is empty or missing");
            yield break;
        }
    }
}

public class DebugSettings : MonoBehaviour
{
    public DebugLevelLoader debugLevel;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (debugLevel != null)
        {
            if (Input.GetKeyDown(debugLevel.loadLevelKey))
            {
                Debug.Log("Loading Debug Level: " + debugLevel.levelName);
                StartCoroutine(debugLevel.LoadSceneAsyncRoutine());
            }
        }
    }
}