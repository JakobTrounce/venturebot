﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

    public static Vector2 Position2D(this Transform transform)
    {
		Vector2 returnThis = new Vector2(transform.position.x, transform.position.y);
        return returnThis;
    }
	
}
