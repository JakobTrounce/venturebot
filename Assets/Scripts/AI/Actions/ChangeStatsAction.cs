﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/ChangeStats")]
public class ChangeStatsAction : AIAction
{
    public EnemyStats stats;
    public override void Act(Statecontroller controller)
    {
        ChangeStats(controller);
    }

    void ChangeStats(Statecontroller controller)
    {
        controller.enemyStats.stats = stats;
        controller.navMeshAgent.speed = stats.moveSpd;
    }
}
