﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Dash")]
public class DashAction : AIAction
{
    public float dashSpeed;

	private float timer = 0.0f;

	public bool isDashing;
    public override void Act(Statecontroller controller)
    {
        Debug.Log(isDashing);
        Dash(controller);
    }

	private void Awake()
	{
        isDashing = true;
    }
	private void Dash(Statecontroller controller)
    {

        controller.navMeshAgent.SetDestination(controller.GetLastPlayerPos());
		if (!isDashing)
		{
            if (Vector3.Distance(controller.navMeshAgent.transform.position, controller.GetLastPlayerPos()) >= 2f)
            {
                controller.navMeshAgent.speed = 1f;
                controller.navMeshAgent.acceleration = 1f;
                isDashing = true;
            }
		}
		else if (isDashing)
		{
            if (Vector3.Distance(controller.navMeshAgent.transform.position, controller.GetLastPlayerPos()) <= 2f)
            {
                controller.navMeshAgent.speed = 0;
                controller.navMeshAgent.acceleration = 0;
                isDashing = false;
            }
            else
            {
                if (dashSpeed <= 0) controller.navMeshAgent.speed = 1f;
                if (controller.navMeshAgent.speed <= 0) controller.navMeshAgent.speed = 1f;
                if (controller.navMeshAgent.acceleration <= 0) controller.navMeshAgent.acceleration = 1f;
                controller.navMeshAgent.speed += dashSpeed;
                controller.navMeshAgent.acceleration *= dashSpeed;
            }
		}
    }
}
