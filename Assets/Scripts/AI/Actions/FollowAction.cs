﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Follow")]
public class FollowAction : AIAction
{
    private Transform _playerTransform;

    public override void Act(Statecontroller controller)
    {

        Follow(controller);
    }

    private void Follow(Statecontroller controller)
    {
        controller.navMeshAgent.SetDestination(controller.player.position);
    }
}
