﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName = "PluggableAI/Actions/Idle")]
public class IdleAction : AIAction
{
	public float desiredTimer;
   
    public override void Act(Statecontroller controller)
    {

		Idle(controller);
        
    }

	void Idle(Statecontroller controller)
	{
        float timer = 0f;
		while( timer < desiredTimer) 
		{
            timer += Time.deltaTime;
		}
	}
}
