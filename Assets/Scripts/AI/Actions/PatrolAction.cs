﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Patrol")]
public class PatrolAction : AIAction
{
    public override void Act(Statecontroller controller)
    {
        controller.navMeshAgent.speed = controller.enemyStats.stats.moveSpd;
        Patrol(controller);
    }

    private void Patrol(Statecontroller controller)
    {
        if(controller.nextWayPoint == -1)
        {
            return;
        }
        if(controller.nextWayPoint > controller.wayPointList.Capacity)
        {
            return;
        }
        controller.navMeshAgent.destination = controller.wayPointList[controller.nextWayPoint].position;
        controller.navMeshAgent.Resume();

        if(controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            controller.nextWayPoint = (controller.nextWayPoint + 1)%controller.wayPointList.Count; // use modulus to make sure we dont exceed the count, and if we do it just cycles back to beginning
        }
    }

}
