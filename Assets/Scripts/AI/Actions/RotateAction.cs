﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Rotate")]
public class RotateAction : AIAction
{
    public override void Act(Statecontroller controller)
    {
        Rotate(controller);
    }

    void Rotate(Statecontroller controller)
    {
        controller.transform.Rotate(0, 180, 0);
    }
}
