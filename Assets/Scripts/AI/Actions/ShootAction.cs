﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Shoot")]
public class ShootAction : AIAction
{
    public GameObject bulletPrefab;
    private Ammo ammo;
    [HideInInspector] public Vector2 dirToTarget;
    float timer = 0.0f;



    public override void Act(Statecontroller controller)
    {
       //controller.transform.LookAt((controller.player.position - controller.transform.position));
       Shoot(controller);
    }

    private void Shoot(Statecontroller controller)
    {
        // Get the direction vector between enemy and player, shoot in that direction
        Debug.DrawRay(controller.enemy.transform.position, controller.enemy.transform.forward * controller.enemyStats.stats.bulletRange, Color.yellow);
        dirToTarget = (controller.player.Position2D() - controller.transform.Position2D()).normalized;

        if (timer >= controller.enemyStats.stats.attackDelay)
        {
            Fire(dirToTarget, controller);
            timer = 0.0f;
        }
        timer += Time.deltaTime;
    }

    void Fire(Vector2 direction, Statecontroller controller)
    {
        Transform spawnPos = controller.enemyStats.bulletSpawnPos;
        spawnPos.rotation.SetLookRotation(direction);

        var bullet = controller.objPoolManager.GetObjectFromPool("Bullet");
        if (bullet == null)
       {
           return;
       }

       bullet.transform.position = spawnPos.position;
       bullet.transform.rotation = spawnPos.rotation;
       bullet.SetActive(true);
		bullet.tag = "EnemyBullet";
       ammo = bullet.GetComponent<Ammo>();
       ammo.Initialize(controller.enemyStats.stats.bulletDmg, direction);
    }
}
