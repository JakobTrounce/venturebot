﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/SplitAction")]
public class SplitAction : AIAction
{
    
    [Tooltip("The enemy that you're current enemy will split into (must be a prefab)")]
    public GameObject nextEnemy;
    public int NumberOfChildren;
    public override void Act(Statecontroller controller)
    {
        Split(controller);
    }


    private void Split(Statecontroller controller)
    {
        for (int i = 0; i < NumberOfChildren; i++)
        {
            Vector2 pos = controller.enemy.transform.Position2D() + new Vector2(i - 1, 0);
             GameObject enemy = Instantiate(nextEnemy,pos, Quaternion.identity);
            enemy.tag = "Enemy";
        }
    }
}


