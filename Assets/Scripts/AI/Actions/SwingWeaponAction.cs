﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/SwingWeapon")]
public class SwingWeaponAction : AIAction
{

    public enum SwingDirection
    {
        Forward,
        Backward,
    };
    private SwingDirection dir;

    [Tooltip("The weapon that will be used")]
    public GameObject weapon;
    [Tooltip("where the weapon should stop swinging")]
    public float stopPoint;
    public float swingTime;
    private bool isShielding;
    private bool hitStopPoint;
    private bool shouldSwingForward = true;

    private void Awake()
    {
        dir = SwingDirection.Forward;
        hitStopPoint = false;
        Debug.Log("<color=lime>Current Weapon Z: </color>" + weapon.transform.eulerAngles.z);
    }

    public override void Act(Statecontroller controller)
    {
        weapon = GameObject.FindGameObjectWithTag("Weapon");
        SwingWeapon(controller);
    }

    private void SwingWeapon(Statecontroller controller)
    {
        float zRot = weapon.transform.eulerAngles.z;
        switch (dir)
        {
            case SwingDirection.Forward:
                if (zRot <= 90.0f || zRot >= 355.0f)
                {
                    weapon.transform.Rotate(0, 0, 1);
                }
                else
                {
                    dir = SwingDirection.Backward;
                }
                break;
            case SwingDirection.Backward:
                if (zRot <= 355.0f)
                {
                    weapon.transform.Rotate(0, 0, -1);
                }
                else
                {
                    dir = SwingDirection.Forward;
                }
                break;
        }
    }
}
