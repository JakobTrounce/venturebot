﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/DamageDecision")]
public class DamageDecision : Decision {

	public float hpTrigger;
	public override bool Decide(Statecontroller controller)
	{
		return DamageTaken(controller);
	}
	
	 private bool DamageTaken(Statecontroller controller)
	{
		if(controller.enemyStats.currentHealth <= hpTrigger)
			return true;
		else
			return false;
	}
}
