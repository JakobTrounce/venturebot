﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/Death")]
public class DeathDecision : Decision
{
    public override bool Decide(Statecontroller controller)
    {
        return IsDead(controller);
    }

    private bool IsDead(Statecontroller controller)
    {
        if(controller.enemyStats.currentHealth <= 10)
        {
            controller._isDead = true;
            return true;
        }
        else
        {
            return false;
        }
    }

}
