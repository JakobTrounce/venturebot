﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/Look")]
public class LookDecision : Decision
{
    public float FOV = 70.0f;
    public override bool Decide(Statecontroller controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }
   
    private bool Look(Statecontroller controller)
    {
		float halfFOV;
		if (FOV > 0)
		{
			 halfFOV = FOV/2;
		}
		else
		{
			halfFOV = 0f;
		}
        Quaternion leftRayRotation = Quaternion.AngleAxis(-halfFOV, Vector2.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(halfFOV, Vector2.up);
        Vector2 leftRayDirection = leftRayRotation * controller.transform.forward;
        Vector2 rightRayDirection = rightRayRotation * controller.transform.forward;

        Debug.DrawRay(controller.transform.position, leftRayDirection* controller.enemyStats.stats.aggroRange, Color.blue);
        Debug.DrawRay(controller.transform.position, rightRayDirection* controller.enemyStats.stats.aggroRange, Color.blue);




        Vector2 direction = (controller.player.position - controller.enemy.transform.position);
        float distance = Vector2.Distance(controller.transform.position, controller.player.position);
        float cone = Vector2.Angle(direction,controller.enemy.transform.forward);
        if (cone < FOV && distance <= controller.enemyStats.stats.aggroRange) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
