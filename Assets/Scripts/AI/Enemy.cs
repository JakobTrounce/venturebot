﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform bulletSpawnPos;
    public EnemyStats stats;
    public float currentHealth;

    // Use this for initialization
    void Start()
    {
        Initialize();
    }
    private void Update()
    {
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
    }


    private void Die()
    {
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {

    }

    private void Initialize()
    {
        currentHealth = stats.enemyHealth;
    }
}
