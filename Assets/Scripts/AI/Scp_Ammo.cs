﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Ammo : MonoBehaviour
{
    private Rigidbody rb;
    Renderer rend;

    public float speed = 1f; // default speed 1 unit / second
    public float distance = 5f; // default distance 5 units
    public Transform boomerang; // the object you want to throw (assign from the scene)
    private float _distance; // the distance it moves
    private bool _back; // is it coming back

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody>();
        _distance = 0; // resetting the distance
        _back = false; // resetting direction
    }

    // Update is called once per frame
    void Update()
    {
        //float travel = Time.deltaTime * speed;
        //if (!_back)
        //{
        //    rb.velocity = transform.forward * travel; // moves object
        //    _distance += travel; // update distance
        //    _back = _distance >= distance; // goes back if distance reached
        //}
        //else
        //{
        //    rb.velocity = transform.forward * -travel; // moves object
        //    _distance -= travel; // update distance;
        //}
        //rb.velocity = transform.forward * 10.0f; // Default bullet;
        rb.AddForce(transform.forward * 4f);// Default rocket;
        Debug.DrawRay(transform.position, transform.forward * 5, Color.red, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
