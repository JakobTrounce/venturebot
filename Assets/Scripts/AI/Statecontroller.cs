﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Statecontroller : MonoBehaviour
{



    public State currentState;
    public float enemyHealth;
    public GameObject enemy;
    public Transform player;
    public State remainState;
    public List<Transform> wayPointList;

    [HideInInspector] public bool _isDead = false;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public Enemy enemyStats;
    [HideInInspector] public int nextWayPoint;
    [HideInInspector] public Transform chaseTarget;
    [HideInInspector] public ObjectPoolManager objPoolManager;


    private bool aiActive;
    private LevelManager lvlManager;
	private Transform playerLastPosition;
	private float timer;

	void Awake()
    {
        var lvlMgrObj = GameObject.FindWithTag("LevelManager");
        lvlManager = lvlMgrObj.GetComponent<LevelManager>();

        enemyStats = GetComponent<Enemy>();
        if(enemyStats == null)
        {
            Debug.LogError("Couldnt Find EnemyScript");
        }
        enemyHealth = enemyStats.stats.enemyHealth;
        navMeshAgent = GetComponent<NavMeshAgent>();
		//playerLastPosition.position = this.transform.position;
	}
    void Start()
    {
        aiActive = true;
        objPoolManager = ServiceLocator.Get<ObjectPoolManager>();
        if (objPoolManager == null)
        {
            Debug.LogError("couldnt find object pool");
        }
	}


    void Update()
    {
       
        // TODO - Please Fix This.
        if (lvlManager == null)
        {
            var lvlMgrObj = GameObject.FindWithTag("LevelManager");
            lvlManager = lvlMgrObj.GetComponent<LevelManager>();
        }

        if (player != null)
        {
            player = lvlManager.GetPlayer().transform;
        }

        if (player == null)
        {
            if (lvlManager == null)
            {
                Debug.LogError("levelManager not found");
                return;
            }
            player = lvlManager.GetPlayer().transform;
        }

        if(_isDead)
        {
            gameObject.SetActive(false);
        }
        if (!aiActive || currentState == null)
        {
            return;
        }
        else
        {
            currentState.UpdateState(this);
        }

		if (timer >= 2f)
		{
			//playerLastPosition.position = player.position;
			timer = 0f;
		}

		timer += Time.deltaTime;
        Debug.Log(currentState);
    }



    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
        }
    }

    void TakeDamage(float dmg)
    {
        enemyHealth -= dmg;
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.CompareTag("Player"))
		{
			navMeshAgent.acceleration = 0;
			navMeshAgent.velocity = new Vector3(0f,0f,0f);
			navMeshAgent.SetDestination(wayPointList[0].position);
		}
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			navMeshAgent.acceleration = 0;
			navMeshAgent.velocity = new Vector3(0f, 0f, 0f);
		}
	}

	private void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			navMeshAgent.acceleration = 0;
			navMeshAgent.velocity = new Vector3(0f, 0f, 0f);
		}
	}

	public Vector3 GetLastPlayerPos()
	{
		return player.position;
	}
}
