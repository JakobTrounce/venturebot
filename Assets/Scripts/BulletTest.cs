﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTest : MonoBehaviour
{
    //void OnCollisionEnter(Collision collision)
    //{
    //    var hit = collision.gameObject;
    //    var health = hit.GetComponent<Health>();
    //    if(health != null)
    //    {
    //        health.TakeDamage(10);           
    //    }
    //    Destroy(gameObject);
    //}

    public void OnTriggerStay(Collider other)
    {
        var hit = other.gameObject;
        var health = hit.GetComponent<Health>();
        if (health != null)
        {
            health.TakeDamage(10);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
