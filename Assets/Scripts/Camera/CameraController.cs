﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // public
    [SerializeField]
    private GameObject _player;

    public float trailTimer;
    public float cameraSpeed;
    public float offset;

    public bool IsInitialized { get { return _isInitialized; } }
    private bool _isInitialized = false;

    // private
    private float timerSet;
    private float MoveTimer;
    private Vector2 lastPlayerPos;
    private Vector2 trailDistance;
    private Vector2 CameraPos;

    public void Initialize(GameObject player)
    {
        CameraPos = transform.position - player.transform.position;
        timerSet = trailTimer;
        MoveTimer = trailTimer;
        trailDistance = new Vector2(0, 0);

        _player = player;

        _isInitialized = true;
    }

    // Update is called once per frame
    void Update ()
    {
        if (!IsInitialized)
        {
            return;
        }

        if(isPlayerMoving(lastPlayerPos)&& trailTimer > 0f)
        {
            if(_player.transform.position.x > lastPlayerPos.x)
            {
                trailDistance.x += Time.deltaTime*cameraSpeed;
            }
            else if(_player.transform.position.x < lastPlayerPos.x)
            {
                trailDistance.x -= Time.deltaTime*cameraSpeed;
            }
            if(_player.transform.position.y > lastPlayerPos.y)
            {
                trailDistance.y += Time.deltaTime*cameraSpeed;
            }
            else if(_player.transform.position.y < lastPlayerPos.y)
            {
                trailDistance.y -= Time.deltaTime*cameraSpeed; 
            }

            trailTimer -= Time.deltaTime*cameraSpeed;
        }
        else if (!isPlayerMoving(lastPlayerPos) && trailTimer < timerSet)
        {
            if(trailDistance.x > 0)
                trailDistance.x -= Time.deltaTime*cameraSpeed;
            if (trailDistance.x < 0)
                trailDistance.x += Time.deltaTime*cameraSpeed;
            if (trailDistance.y > 0)
                trailDistance.y -= Time.deltaTime*cameraSpeed;
            if (trailDistance.y < 0)
                trailDistance.y += Time.deltaTime*cameraSpeed;

            trailTimer += Time.deltaTime;
        }
        transform.position = new Vector3((_player.transform.position.x - trailDistance.x), _player.transform.position.y + offset - trailDistance.y, -5);
        lastPlayerPos = _player.transform.position;
	}

    private void FixedUpdate()
    {

    }




    bool isPlayerMoving(Vector2 lastPos)
    {
       // if (_player.transform.position == lastPos)
       // {
            return false;
        //}
        
    }
}
