﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tremor : MonoBehaviour {

    private Vector2 Origin { get; set; }
    bool atOrigin;

    float intensity, duration;



	// Use this for initialization
	void Start () {
        //intensity = duration = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(atOrigin)
        {
            Origin = transform.position;
            Vector3 adjustor = new Vector3(Random.Range(-1f, 1f) * intensity, Random.Range(-1f, 1f) * intensity);

            transform.position += adjustor;
            atOrigin = !atOrigin;
        }
        else
        {
            transform.position = Origin;
        }
	}
    void Zero()
    {
        intensity = duration = 0;
    }
    public void SetTremor(float i, float d)
    {
        intensity = i;
        duration = d;

        Invoke("Zero", duration);
    }
}
