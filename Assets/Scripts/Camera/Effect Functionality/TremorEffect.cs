﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Plan
// save origin of camera relative to rig


public class TremorEffect : MonoBehaviour
{
    Vector2 originalPos;

    public float duration;
    public float maxDuration;
    public float intensity;
    
    void OnEnable ()
    {
        originalPos = transform.localPosition;
    }

    public void Tremor (float i, float d )
    {
        if ( intensity == 0f )
            intensity = i;
        else
            intensity += i;

        duration += d;
        duration = Mathf.Clamp ( duration, 0, maxDuration );
    }

    void Update ()
    {
        if ( duration > 0 )
        {
            transform.localPosition = originalPos + (Vector2)Random.insideUnitCircle * intensity;
            duration -= Time.deltaTime;
            intensity -= intensity * Time.deltaTime;
        }
        else
        {
            intensity = duration = 0f;
            transform.localPosition = originalPos;
        }
    }
}
