﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTracking : MonoBehaviour {

    [SerializeField]
    private GameObject player;

    [Range ( 0.1f, 10f )]
    public float latchSpeed;

	// Use this for initialization
	void Start () {
        if ( player == null )
        {
            player = GameObject.FindGameObjectWithTag ( "Player" );
        }
	}
	
	// Update is called once per frame
	void Update () {
        if ( player != null )
        {
            Interpolate ();
        }
	}

    void Interpolate ()
    {
        transform.position = Vector2.Lerp ( transform.position, player.transform.position, Time.deltaTime * latchSpeed );
    }
}
