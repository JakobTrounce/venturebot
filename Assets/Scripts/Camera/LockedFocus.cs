﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedFocus : MonoBehaviour
{

    private GameObject player;

    // Use this for initialization
    void Start ()
    {
        LatchToPlayer ();
    }

    // Update is called once per frame
    void Update ()
    {
        if ( player == null )
        {
            Debug.Log ( "Camera - LockedFocus - Player not found" );
            LatchToPlayer ();
        }

        Tracking ();
    }
    void LatchToPlayer ()
    {
        player = GameObject.FindGameObjectWithTag ( "Player" );
    }
    void Tracking ()
    {
        transform.position = new Vector2(player.transform.position.x, player.transform.position.y);
    }
}
