﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionalCamera : MonoBehaviour {

    public bool IsInitialized
    {
        get
        {
            return _isInitialized;
        }
    }
    private bool _isInitialized = false;
    [SerializeField]
    private GameObject _trackedObj;




    [Range ( 0.1f, 10f )]
    public float latchSpeed;

    // Use this for initialization
    public void Initialize ( GameObject player )
    {

        _trackedObj = player;

        _isInitialized = true;
    }

    // Update is called once per frame
    void Update ()
    {
        if (_isInitialized)
        {
            Interpolate();
        }
    }

    void Interpolate ()
    {
        transform.position = Vector3.Lerp ( transform.position, _trackedObj.transform.position, Time.deltaTime * latchSpeed );
    }
    public void UpdateTrackedObject ( GameObject obj )
    {
        _trackedObj = obj;
    }

    public void Tremor ()
    {
        GetComponentInChildren<TremorEffect> ().Tremor(.25f, .5f);
    }


}
