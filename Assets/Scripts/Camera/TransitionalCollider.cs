﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionalCollider : MonoBehaviour {

    public GameObject lockPoint;
    private TransitionalCamera cameraRig;

	void Start () {
        lockPoint = transform.Find ( "LockPoint" ).gameObject;
        cameraRig = GameObject.FindGameObjectWithTag ( "MainCamera" ).transform.root.gameObject.GetComponent<TransitionalCamera>();
        if ( cameraRig == null )
        {
            gameObject.SetActive ( false );
        }
	}

    void OnTriggerEnter2D ( Collider2D other )
    {
        if ( other.CompareTag("Player") )
        {
            cameraRig.UpdateTrackedObject ( lockPoint );
        }
    }
    void OnTriggerExit2D ( Collider2D other )
    {
        if ( other.CompareTag ( "Player" ) )
        {
            cameraRig.UpdateTrackedObject ( other.gameObject );
        }
    }

}
