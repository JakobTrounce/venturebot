﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameDefs 
{
    public static readonly float defaultMusicLevel = 100.0f;

    public static class PlayerSaveInfo
    {
        public static readonly string PLAYER_HEALTH_KEY = "player_health";
    }
    public static class InventoryKeys
    {
        public static readonly string PRIMARY_AMMO_COUNT = "primage_ammo_count";
    }
}
