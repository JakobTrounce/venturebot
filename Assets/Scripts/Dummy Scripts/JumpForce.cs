﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpForce : MonoBehaviour {

    private Rigidbody myRB;
    public float jumpForce;

    void Start ()
    {
        myRB = GetComponent<Rigidbody> ();
    }
	// Update is called once per frame
	void Update () {
        if ( Input.GetAxis ( "Action" ) != 0 )
        {
            myRB.AddExplosionForce(jumpForce, transform.position - Vector3.up, 1.0f, jumpForce );
        }
	}
}
