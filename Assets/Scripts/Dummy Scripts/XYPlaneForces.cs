﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XYPlaneForces : MonoBehaviour {

    private Rigidbody objRigidBody;

    [SerializeField]
    private float speed;

    void Start ()
    {
        objRigidBody = GetComponentInChildren<Rigidbody> ();
    }

    // Update is called once per frame
    void Update ()
    {
        MovementHandler ();
    }

    void MovementHandler ()
    {
        XAxisHandler ();
        ZAxisHandler ();
    }

    // Axis checks
    void XAxisHandler ()
    {
        // Store current axis value
        float axisValue = Input.GetAxis ( "Horizontal" );

        if ( axisValue != 0f )
        {
            // add axis value to GameObject position
            objRigidBody.AddForce ( (Vector3.right * axisValue) * (Time.deltaTime * speed) );
        }
    }
    void ZAxisHandler ()
    {
        // Store current axis value
        float axisValue = Input.GetAxis ( "Vertical" );

        if ( axisValue != 0f )
        {
            // add axis value to GameObject position
            objRigidBody.AddForce ( ( Vector3.forward * axisValue ) * ( Time.deltaTime * speed ) );
        }
    }
}
