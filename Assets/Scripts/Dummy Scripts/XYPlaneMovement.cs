﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XYPlaneMovement : MonoBehaviour {

    [SerializeField]
    private float speed;

	// Update is called once per frame
	void Update () {
        MovementHandler ();
	}

    void MovementHandler ()
    {
        XAxisHandler ();
        ZAxisHandler ();
    }

    // Axis checks
    void XAxisHandler ()
    {
        // Store current axis value
        float axisValue = Input.GetAxis ( "Horizontal" );

        if ( axisValue != 0f)
        {
            // add axis value to GameObject position
            transform.position += (Vector3.right * axisValue ) * ( Time.deltaTime * speed );
        }
    }
    void ZAxisHandler ()
    {
        // Store current axis value
        float axisValue = Input.GetAxis ( "Vertical" );

        if ( axisValue != 0f )
        {
            // add axis value to GameObject position
            transform.position += (Vector3.forward * axisValue) * ( Time.deltaTime * speed );
        }
    }
}
