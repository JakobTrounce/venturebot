﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndOfLevel : MonoBehaviour {

    public bool dash;
    public bool doubleJump;
    public bool doubleDash;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            
            PlayerController p = other.GetComponent<PlayerController>();
            
            if(SceneManager.GetActiveScene().name == "Denial")
            {
                doubleJump = true;
                p.DoubleJump = doubleJump;
            }
            if (SceneManager.GetActiveScene().name == "Anger")
            {
                doubleJump = true;
                dash = true;
                p.DoubleJump = doubleJump;
                p.HasDash = dash;
            }
            if (SceneManager.GetActiveScene().name == "Whothefuckknows")
            {
                doubleJump = true;
                dash = true;
                doubleDash = true;
                p.DoubleJump = doubleJump;
                p.HasDash = dash;
                p.DoubleDash = doubleDash;
            }
            p.gameObject.SetActive(false);
            SceneManager.LoadSceneAsync("Overworld");
        }
    }
}
