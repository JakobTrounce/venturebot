﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    public GameObject[] enemyAIType;
    public GameObject[] enemySpawnNumber;

    public float spawnInterval;

    private bool spawningEnabled = false;

    void Start()
    {
        
    }

    void Update()
    {

    }

    private IEnumerator SpawnEnemyRoutine()
    {
        while (spawningEnabled)
        {
            yield return new WaitForSeconds(spawnInterval);
        }
    }
}
