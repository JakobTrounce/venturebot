﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu (menuName = "DefaultEnemyStats")]
public class EnemyStats : ScriptableObject
{

    public string enemyName;
    public float enemyHeight;
    public float enemyHealth;
    public float moveSpd;
    public float aggroRange;
    public BulletType bulletType;
    public float bulletDmg;
    public float fireRate; // THIS IS NFG -LW
    public float bulletRange;
    public float attackDelay;

  

}
