﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public bool enableBootstrapLoader = true;
    public LevelManager levelManager;

    private int _sceneIndex = -1;
    public int SceneIndexToLoad { get { return _sceneIndex; } }

    private static bool _loadingStarted = false;
    private static bool _gameLoaderComplete = false;

    private void Awake()
    {
        if (GameLoader.Complete)
        {
            Initialize();
            return;
        }

        if (enableBootstrapLoader && !_loadingStarted)
        {
            _loadingStarted = true;
            _sceneIndex = SceneManager.GetActiveScene().buildIndex;
            GameLoader.CallOnComplete(Initialize);
            SceneManager.sceneLoaded += OnLevelLoaded;
            SceneManager.LoadSceneAsync("Boot", LoadSceneMode.Single);
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void OnGameLoaderComplete()
    {
        _gameLoaderComplete = true;
    }

    private void Initialize()
    {
        var lvlManagerObj = GameObject.FindGameObjectWithTag("LevelManager");
        if (lvlManagerObj != null)
        {
            levelManager = lvlManagerObj.GetComponent<LevelManager>();
            levelManager.Initialize();
        }
    }

    private void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == SceneIndexToLoad && _gameLoaderComplete)
        {
            Initialize();
        }
    }
}