﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject playerSpawner;
    public GameObject cameraRig;

    private TransitionalCamera _cameraController;
    private PlayerSpawn _playerSpawn;
    private GameObject _player;
	private SpawnManager mSpawnManager;

    // Should know about all enemy spawners.
    // Add all enemies to a list here.

    //UI STORING THE LEVEL NAME -Zayn
    public string currentLevelName;
	//public string savedLevelName;


	// Temporary _ Zenbe_ To fix bugs null reference for Player controller
	//public bool DoubleJump;
	//public bool DoubleDash = true;
	//public bool HasDash = true;




	public void Initialize()
    {
        Debug.Log("LevelManager::Initialize()");
        //UI STORING THE LEVEL NAME -Zayn
        currentLevelName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

        if (cameraRig == null)
        {
            Debug.LogError("Camera Rig Not Found!");
        }

        mSpawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
		if(mSpawnManager == null)
		{
			Debug.LogError("SpawnManager not found");
		}

		_playerSpawn = mSpawnManager.GetFirstSpawn();
		if (_playerSpawn == null)
		{
			Debug.LogError("Player Spawner Not Found!");
			return;
		}

		_cameraController = cameraRig.GetComponent<TransitionalCamera>();

        _playerSpawn.SpawnPlayer();
        _player = _playerSpawn.Player;
        if(_player == null)
        {
            _playerSpawn.SpawnPlayer();
            _player = _playerSpawn.Player;
        }
        _cameraController.Initialize(_player);
    }

	private void Update()
	{
        if(!_player.activeInHierarchy)
        {
             mSpawnManager.SpawnPlayer();
        }
    }




	public GameObject GetPlayer()
    {
        return _player;
    }
}
