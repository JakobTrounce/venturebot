﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
	public PlayerSpawn[] spawnerList;
	private PlayerSpawn mCurrentSpawner;
	private int spawnerCount = 0;
    private int _deathCount = 1;
	// Use this for initialization

	// if you get to a checkpoint, you can die once and go back to it


	void Awake()
	{
		spawnerList[0].isActiveSpawner = true;
		mCurrentSpawner = spawnerList[0];
	}

	// Update is called once per frame
	void Update()
	{
        if (spawnerCount < 0) spawnerCount = 0;
        if (spawnerList[spawnerCount + 1].spawnerHit)
		{
			spawnerList[spawnerCount].SetInactive();
			spawnerList[spawnerCount + 1].isActiveSpawner = true;
            mCurrentSpawner = spawnerList[spawnerCount + 1];
            spawnerCount++;
		}
         if (_deathCount >  1)
        {
            spawnerList[spawnerCount].SetInactive();
            spawnerList[spawnerCount - 1].SetActive();
            mCurrentSpawner = spawnerList[spawnerCount - 1];
            spawnerCount-=2;
            _deathCount = 0;
        }

    }

    public void SpawnPlayer()
    {
        _deathCount++;

        mCurrentSpawner.SpawnPlayer();
    }
	public PlayerSpawn CurrentSpawner()
	{
		return mCurrentSpawner;
	}

	public PlayerSpawn GetFirstSpawn()
	{
		return spawnerList[0];
	}
}
