﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndOfLevelTrigger : MonoBehaviour
{

    public bool doubleJump;
    public bool dash;
    public bool doubleDash;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            LevelLoader lm = GameObject.FindGameObjectWithTag("LevelLoader").GetComponent<LevelLoader>();
            if (lm == null)
            {
                Debug.LogError("Levelmanager not found");
            }

          
            SceneManager.LoadSceneAsync(2);
        }
    }
}
