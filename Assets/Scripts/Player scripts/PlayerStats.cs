﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PlayerStats")]
public class PlayerStats : ScriptableObject
{
    public float playerHeight;
    public float PlayerHealth;
    public float moveSpd;
    public float JumpHeight;
    public float damage;
    public Vector2 Drag;
    public float walkAcceleration;
    public float groundDeceleration;
    public float airAcceleration;
}
