﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedBulge : MonoBehaviour {

    ////////////////////////////////////////////////
    // Script variables
    //
    private bool activeNode = false;

    [Tooltip ( "Scaling: Up(n > 1), Down(n < 1)" )]
    [SerializeField]
    private float scaleRatio;

    [Tooltip ( "Time it takes to fully scale up" )]
    [Range ( 0f, 100f )]
    [SerializeField]
    private float scaleRate;


    ////////////////////////////////////////////////
    // User built methods
    //
    private void Transition ( Vector3 scaleVector )
    {
        transform.localScale = Vector3.Lerp ( transform.localScale, scaleVector, scaleRate * Time.deltaTime);
    }


    ////////////////////////////////////////////////
    // Monobehaviour methods
    //
    void Update ()
    {
        if ( activeNode && ( transform.localScale != Vector3.one * scaleRatio ) )
        {
            Transition ( Vector3.one * scaleRatio );
        }
        else if ( !activeNode && ( transform.localScale != Vector3.one ) )
        {
            Transition ( Vector3.one );
        }
    }


    ////////////////////////////////////////////////
    // Trigger methods
    //
    private void OnTriggerEnter ( Collider other )
    {
        if ( other.gameObject.CompareTag( "Player" ) )
        {
            activeNode = true;
        }
    }
    private void OnTriggerExit ( Collider other )
    {
        if ( other.gameObject.CompareTag ( "Player" ) )
        {
            activeNode = false;
        }
    }
}
