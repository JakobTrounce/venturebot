﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObjects : MonoBehaviour
{
    public float percentChance; //must be under 1 and higher than 0
    public GameObject prize;

    public void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            Debug.Log("Colliding");
            if (Random.value <= percentChance)
            {
                Instantiate(prize, transform.position, Quaternion.identity);
            }
        }
    }

    // create an empty game object in front of the player, give it a box collider, use that to check if the player is "colliding" with anything
}
