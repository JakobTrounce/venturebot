﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour
{
    public float damage;
    public PlayerController playerController;



	/* Garbage
    public void OnCollisionEnter2D(Collision2D collision)
    {
		
        if (collision.gameObject.CompareTag("Player"))
        {
            //playerScript.playerHealth -= damage;
        }
    }*/
	public void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.gameObject.CompareTag("Player"))
		{
			playerController = collision.gameObject.GetComponent<PlayerController>();
			playerController.TakeDamage(damage);
		}

	}
}
