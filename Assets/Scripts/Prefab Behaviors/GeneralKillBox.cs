﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralKillBox : MonoBehaviour {

    [Tooltip ( "When selected, collision sets activeState(false)" )]
    [SerializeField]
    private bool memSafe;

    private Collider2D _collider;

    void Start ()
    {
        _collider = GetComponent<BoxCollider2D>();
        if (_collider.isTrigger == false)
            _collider.isTrigger = true;
    }
    
    void CollisionHandler ( Collider2D other )
    {
        if ( memSafe )
        {
            other.gameObject.transform.root.gameObject.SetActive ( false );
        }
        else
        {
            Destroy ( other.gameObject.transform.root.gameObject );
            
        }
    }
    void OnTriggerEnter2D ( Collider2D other )
    {
        CollisionHandler ( other );
    }
}
