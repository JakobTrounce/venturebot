﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IcicleData
{
    public GameObject icicle;
    public Transform spawnLocation;
}
public class Activating_IcicleTrap : MonoBehaviour
{
    public List<IcicleData> trapData;
    public float spawnInterval;

    public bool SpawningEnabled { get { return spawningEnabled; } set { spawningEnabled = value; } }
    private bool spawningEnabled = false;

    public void activateSpawner()
    {
        spawningEnabled = true;
        StartCoroutine("SpawnEnemyRoutine");
    }

    public void deactivateSpawn()
    {
        spawningEnabled = false;
        StopCoroutine("SpawnEnemyRoutine");
    }

    private IEnumerator SpawnEnemyRoutine()
    {
        while (spawningEnabled)
        {
            yield return new WaitForSeconds(spawnInterval);
            foreach (var spawner in trapData)
            {
                Instantiate(spawner.icicle, spawner.spawnLocation.position, Quaternion.identity);
            }
        }
    }
}
