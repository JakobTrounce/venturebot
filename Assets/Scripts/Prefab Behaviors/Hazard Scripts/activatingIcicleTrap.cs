﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activatingIcicleTrap : MonoBehaviour
{
    public Activating_IcicleTrap trap;
    public Transform trigger;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            trap.activateSpawner();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, trigger.lossyScale);
    }
}
