﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class icicle : MonoBehaviour
{
    public float icicleDamage;
    public Player playerScript;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (playerScript.playerHealth > 0)
            {
                playerScript.playerHealth -= icicleDamage;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
