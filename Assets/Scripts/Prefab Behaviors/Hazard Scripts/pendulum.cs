﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pendulum : MonoBehaviour
{
    Quaternion start, end;

    [SerializeField, Range(0.0f, 360f)]
    private float angle = 90.0f;

    [SerializeField, Range(0.0f, 5f)]
    private float speed = 2.0f;

    [SerializeField, Range(0.0f, 10f)]
    private float startTime = 0.0f;

    void Start()
    {
        start = PendulumRotation(angle);
        end = PendulumRotation(-angle);
    }

    private void FixedUpdate()
    {
        startTime += Time.deltaTime;
        transform.rotation = Quaternion.Lerp(start, end, (Mathf.Sin(startTime * speed + Mathf.PI / 2) + 1.0f) / 2.0f);
    }

    public void ResetTimer()
    {
        startTime = 0.0f;
    }

    Quaternion PendulumRotation(float angle)
    {
        var pendulumRotation = transform.rotation;
        var angleX = pendulumRotation.eulerAngles.x + angle;

        if (angleX > 180)
        {
            angleX -= 360;
        }
        else if (angle < 180)
        {
            angleX += 360;
        }

        pendulumRotation.eulerAngles = new Vector3(angleX, pendulumRotation.eulerAngles.y, pendulumRotation.eulerAngles.z);
        return pendulumRotation;
    }
	
}
