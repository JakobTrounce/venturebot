﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spike : MonoBehaviour
{
    public float spikeDamage;


    public Rigidbody2D rb;
    public float fallSpeed;

    public bool doesDespawn;

	private GameObject Self;
	

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
		Self = this.gameObject;
    }

    private void Update()
    {
        if(!rb.isKinematic)
        {
            Vector3 direction = Vector3.down;
            rb.velocity = direction * fallSpeed;
        }		
	}   

   public void OnTriggerEnter2D(Collider2D collision)
    {
		Debug.Log("Trigger");

		Debug.Log(string.Format("Spike name: {0}", collision.name));

        if (collision.gameObject.CompareTag("Player"))
        {
			PlayerController p = collision.GetComponent<PlayerController>();
			p.TakeDamage(spikeDamage);
        }
    }	
}


