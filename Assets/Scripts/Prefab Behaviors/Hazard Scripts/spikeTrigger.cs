﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spikeTrigger : MonoBehaviour
{
	public GameObject spike;
	// Commented by Erik Ross
	// public Rigidbody2D rb;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
			print("Im triggering on this one");
			spike.GetComponent<Rigidbody2D>().isKinematic = false;
        }
    }
}
