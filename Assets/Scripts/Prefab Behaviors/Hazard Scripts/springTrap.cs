﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class springTrap : MonoBehaviour
{
    [System.Serializable]
    public enum ForceMode
    {
        Additive,
        Instant,
    };
    public ForceMode mode;

    public float springForce;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("<color=cyan>Player Activated Spring Trap</color>");
            PlayerController p = other.gameObject.GetComponent<PlayerController>();
            if (p == null)
            {
                Debug.LogError("Objet Tagged as 'Player' has no Player component attached");
                return;
            }

            Vector2 force = gameObject.transform.up * springForce;
            p.ApplySpringTrap(force, mode);
        }
    }

    private void OnDrawGizmos()
    {
    }
}
