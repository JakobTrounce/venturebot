﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upwardPassPlatform : MonoBehaviour
{
    public Transform platform;
    private BoxCollider2D _boxCollider;

    private void Awake()
    {
        if (platform == null)
        {
            Debug.LogError("Platform is Null");
            return;
        }

        _boxCollider = platform.GetComponent<Collider2D>() as BoxCollider2D ;
        if (_boxCollider == null)
        {
            Debug.LogError("Platform is missing it's BoxCollider");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Enter");
            _boxCollider.enabled = false;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Exit");
            _boxCollider.enabled = true;
        }
    }
}
