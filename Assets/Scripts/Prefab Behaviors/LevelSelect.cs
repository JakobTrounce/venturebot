﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour
{
    [SerializeField]
    private GameObject[] nodes;
    [SerializeField]
    private Transform player;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < nodes.Length;++i)
        {

            if (IsOnNode(player) && Input.GetButtonDown("Action"))
            {

                RaycastHit hit;
                Physics.Raycast(player.position, Vector2.down, out hit);
                SceneManager.LoadScene(hit.collider.gameObject.name);
                break; 
            }
        }
    }


    private bool IsOnNode(Transform p)
    {
        RaycastHit hit;
        if(Physics.Raycast(p.position,Vector2.down,out hit))
        {
            if(hit.collider.tag == "Node")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}