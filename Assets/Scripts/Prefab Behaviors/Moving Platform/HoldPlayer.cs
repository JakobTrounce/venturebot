﻿using UnityEngine;
using System.Collections;

public class HoldPlayer : MonoBehaviour
{
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = transform.parent;
        }
    }
    
    void OnTriggerExit2D(Collider2D other)
    {       
        other.transform.parent = null;
    }
}