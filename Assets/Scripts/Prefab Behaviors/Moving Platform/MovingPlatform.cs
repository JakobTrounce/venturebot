﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform movingPlatform;
    public Transform positionOne;
    public Transform positionTwo;

    public Vector2 newPosition;
    public string currentState;
    public float platformSpeed = 5.0f;
    public float resetTime;

    private float platformMovementTime;
    private float resetTimer = 0.0f;

	void Start ()
    {
        ChangeTarget();
	}

	void Update ()
    {
        platformMovementTime += Time.deltaTime;
        movingPlatform.position = Vector2.Lerp (movingPlatform.position, newPosition, platformMovementTime / platformSpeed);

        if (Vector2.Distance(movingPlatform.position, positionOne.position) < 0.1f)
        {
            resetTimer += Time.deltaTime;
            if (resetTimer >= resetTime)
            {
                resetTimer = 0.0f;
                ChangeTarget();
            }
        }
        else if (Vector2.Distance(movingPlatform.position, positionTwo.position) < 0.1f)
        {
            resetTimer += Time.deltaTime;
            if (resetTimer >= resetTime)
            {
                resetTimer = 0.0f;
                ChangeTarget();
            }
        }
	}

    public void ChangeTarget()
    {
        if (currentState == "Moving To PositionOne")
        {
            currentState = "Moving To PositionTwo";
            newPosition = positionTwo.position;
            platformMovementTime = 0.0f;
        }
        else if (currentState == "Moving To PositionTwo")
        {
            currentState = "Moving To PositionOne";
            newPosition = positionOne.position;
            platformMovementTime = 0.0f;
        }
        else if (string.IsNullOrEmpty(currentState))  // currentState == null || currentState == ""
        {
            currentState = "Moving To PositionTwo";
            newPosition = positionTwo.position;
            platformMovementTime = 0.0f;
        }
    }
}
