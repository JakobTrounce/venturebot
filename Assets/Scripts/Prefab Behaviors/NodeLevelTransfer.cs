﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class NodeLevelTransfer : MonoBehaviour {

    [Tooltip ( "Name of scene to be loaded (string)" )]
    [SerializeField]
    private string levelName;

    void OnTriggerStay ( Collider other )
    {
        if ( other.gameObject.CompareTag ( "Player" ) )
        {
            if ( Input.GetAxis ( "Action" ) != 0 )
            {
                Debug.Log("Loading Scene: " + levelName);
                SceneManager.LoadScene ( levelName );
            }
        }
    }
}
