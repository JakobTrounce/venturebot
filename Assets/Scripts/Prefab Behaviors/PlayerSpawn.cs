﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

    /// <summary>
    /// In it's current state, this script only works if there is only one spawner in the level. 
    /// </summary>
    /// 

    public GameObject playerPrefab;
    public bool isActiveSpawner;
    public bool spawnerHit;

    private LevelManager _LevelManager;
    public GameObject Player {get{return _playerObj;} set { _playerObj = value; } }
    private GameObject _playerObj;
    private PlayerController _player = null;


    private void Awake()
    {
        _LevelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        if(_LevelManager == null)
        {
            Debug.LogError("[PlayerSpawn] Level manager is null");
        }
        _playerObj = _LevelManager.GetPlayer();
        if(_playerObj == null)
        {
            Debug.LogError("[PlayerSpawn] PlayerObject not found");
        }
    }

    public void SpawnPlayer()
    {
        if (_playerObj == null)
        {
            _playerObj = Instantiate(playerPrefab, transform.position, Quaternion.identity);
            _player = _playerObj.GetComponent<PlayerController>();
            _player.Initialize();
        }
        else
        {
            _playerObj.transform.position = transform.position;
            _playerObj.SetActive(true);
        }
    }

    public void SetActive()
    {
        isActiveSpawner = true;
    }
    public void SetInactive()
    {
        isActiveSpawner = false;
    }

    void Update()
    {
       
    }
    void OnTriggerEnter2D (Collider2D other )
    {
        if ( other.CompareTag("Player") )
        {
            isActiveSpawner = true;
            spawnerHit = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            spawnerHit = false;
        }
    }
}
