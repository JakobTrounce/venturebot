﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float backgroundSize;
    public float zDepth = 0;
    public float parallaxSpeed;

    public bool enableScrolling;
    public bool enableParallax;

    private Transform _cameraTransform;
    private Transform[] _layers;
    private float _lastCameraX;
    private float _viewZone = 10;
    private int _leftIndex;
    private int _rightIndex;

    private void Start()
    {
        _cameraTransform = Camera.main.transform;
        _lastCameraX = _cameraTransform.position.x;
        _layers = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; ++i)
        {
            _layers[i] = transform.GetChild(i);
        }

        _leftIndex = 0;
        _rightIndex = _layers.Length - 1;
    }

    private void Update()
    {
        if (enableParallax)
        {
            float deltaX = _cameraTransform.position.x - _lastCameraX;
            transform.position += Vector3.right * (deltaX * parallaxSpeed);
        }

        _lastCameraX = _cameraTransform.position.x;

        if (enableScrolling)
        {
            if (_cameraTransform.position.x < (_layers[_leftIndex].transform.position.x + _viewZone))
            {
                ScrollLeft();
            }

            if (_cameraTransform.position.x > (_layers[_rightIndex].transform.position.x - _viewZone))
            {
                ScrollRight();
            }
        }
    }

    private void ScrollLeft()
    {
        Vector3 newPos = Vector3.right * (_layers[_leftIndex].position.x - backgroundSize);
        newPos.z = zDepth;

        _layers[_rightIndex].position = newPos;
        _leftIndex = _rightIndex;
        _rightIndex--;
        if (_rightIndex < 0)
        {
            _rightIndex = _layers.Length - 1;
        }
    }

    private void ScrollRight()
    {
        Vector3 newPos = Vector3.right * (_layers[_rightIndex].position.x + backgroundSize);
        newPos.z = zDepth;

        _layers[_leftIndex].position = newPos;
        _rightIndex = _leftIndex;
        _leftIndex++;
        if (_leftIndex == _layers.Length)
        {
            _leftIndex = 0;
        }
    }
}
