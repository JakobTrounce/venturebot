﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnPointGizmo : MonoBehaviour
{
    public Transform spawnPoint;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, 1f);
    }
}
