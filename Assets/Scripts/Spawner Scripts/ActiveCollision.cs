﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCollision : MonoBehaviour {

    public bool active;

	void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            active = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            active = false;
        }
    }
}
