﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivating_Spawner : MonoBehaviour
{
    public Activating_Spawner Spawner;
    public GameObject enemySpawnPoint;

    //Script name that StopSpawning is from game object called spawner
    private Activating_Spawner spawner;

    void Awake()
    {
        spawner = Spawner;
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            enemySpawnPoint.SetActive(false);

            spawner.StopSpawning();
        }
    }
}
