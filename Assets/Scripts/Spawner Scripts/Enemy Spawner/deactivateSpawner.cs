﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deactivateSpawner : MonoBehaviour
{
    public Activating_Spawner Spawner;
    public Transform trigger;

    //Script name that StopSpawning is from game object called spawner
    private Activating_Spawner spawner;

    void Awake()
    {
        spawner = Spawner;
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
            spawner.StopSpawning();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, trigger.lossyScale);
    }
}
