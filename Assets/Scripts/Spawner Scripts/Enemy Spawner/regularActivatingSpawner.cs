﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySpawnerData
{
    public GameObject enemyPrefab;
    public Transform spawnPosition;
}

public class regularActivatingSpawner : MonoBehaviour
{
    public List<EnemySpawnerData> spawnerData;
    public Transform trigger;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" )
        {
            foreach (var spawner in spawnerData)
            {
                Instantiate(spawner.enemyPrefab, spawner.spawnPosition.position, Quaternion.identity);
            }

            gameObject.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, trigger.lossyScale);
    }
}
