﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveMatchCollide : MonoBehaviour {

    [Tooltip ( "GameObject to be activated/deactivated" )]
    [SerializeField]
    private GameObject subject;

    public bool startingActiveState;
    
	// Use this for initialization
	void Start () 
    {
        subject.SetActive ( startingActiveState );
	}

    ////////////////////////////////////////////////
    // Trigger methods
    //
    private void OnTriggerEnter ( Collider other )
    {
        if ( other.gameObject.CompareTag ( "Player" ) )
        {
            subject.SetActive ( !subject.activeSelf );
        }
    }
    private void OnTriggerExit ( Collider other )
    {
        if ( other.gameObject.CompareTag ( "Player" ) )
        {
            subject.SetActive ( !subject.activeSelf );
        }
    }
}
