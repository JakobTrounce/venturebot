﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Billboard : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        // transform.LookAt ( -Camera.main.transform.position, Vector2.up ); // Old Method
        transform.forward = Camera.main.transform.forward;
	}
}
