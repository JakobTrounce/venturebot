﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    [Range(0f, 1f)]
    public float fillAmount;

    public GameObject playerTracker;
    public PlayerController player;
    // Use this for initialization
    void Start ()
    {
        playerTracker = GameObject.FindGameObjectWithTag("Player");
        player = playerTracker.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        fillAmount = player.PlayerHealth() / player.Stats().PlayerHealth;
        transform.localScale = new Vector3(fillAmount, transform.localScale.y, transform.localScale.z);
	}
}
