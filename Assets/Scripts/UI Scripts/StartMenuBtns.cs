﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartMenuBtns : MonoBehaviour {

    // Scene names are required until build order is set in order to load scenes
    [Tooltip ( "Scene name following play button function." )]
    [SerializeField]
    private string playSceneName;

    [Tooltip ( "Scene name following options button function." )]
    [SerializeField]
    private string optionsSceneName;

    // Basic scene loading functionality
    public void PlayButton ()
    {
        SceneManager.LoadScene ( playSceneName );
    }
    public void OptionsButton ()
    {
        SceneManager.LoadScene ( optionsSceneName );
    }
    public void QuitButton ()
    {
        Application.Quit ();
    }
}
