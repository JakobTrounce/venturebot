﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public float bulletSpeed;
    public float dmgMultiplier;

    // This value comes from whatever is shooting this ammo.
    [HideInInspector]public float damage;

    protected Rigidbody2D rb;
    protected Collider2D col;
    protected Vector2 direction = Vector2.zero;

    private bool isInitialized = false;
    
    public bool IsInitialized { get { return isInitialized; } }

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    protected virtual void RecycleObject()
    {
        ServiceLocator.Get<ObjectPoolManager>().RecycleObject(this.gameObject);
    }

    public virtual void Initialize(float dmg, Vector2 dir)
    {
        damage = dmg;
        rb.velocity = new Vector2(0.0f, 0.0f);
        rb.angularVelocity = 0.0f ;
        isInitialized = true;
        direction = dir;
    }
	protected void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			if (CompareTag("EnemyBullet"))
			{
				RecycleObject();
				PlayerController p = collision.gameObject.GetComponent<PlayerController>();
				if (p == null)
				{
					return;
				}
				p.TakeDamage(DamageCalculation());
			}
		}
		else if (collision.gameObject.CompareTag("Enemy"))
		{
			if (CompareTag("PlayerBullet"))
			{
				Enemy e = collision.gameObject.GetComponent<Enemy>();
				if (e == null)
				{
					return;
				}
				e.TakeDamage(DamageCalculation());
				RecycleObject();
			}
			else
			{
				Physics2D.IgnoreCollision(collision.collider,col);
			}
		}
		RecycleObject();
	}

	public float DamageCalculation()
    {
        return damage * dmgMultiplier;
    }
}
