﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Boomerang : Ammo
{
    private Rigidbody rb;
    public float speed = 10f; // default speed 1 unit / second
    public float distance = 10f; // default distance 5 units
    public Transform boomerang; // the object you want to throw (assign from the scene)
    public Transform EnemyTracker;
    private float _distance; // the distance it moves
    private bool _back; // is it coming back

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        _distance = 0; // resetting the distance
        _back = false; // resetting direction
        boomerang = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        EnemyTracker.position = rb.position;
        float travel = Time.deltaTime * speed;
        if (!_back)
        {
            rb.velocity = transform.forward * travel; // moves object
            _distance += travel; // update distance
            _back = _distance >= distance; // goes back if distance reached
        }
        else
        {
            Vector2 direction = (EnemyTracker.position - boomerang.position);
            transform.forward = direction;
            //rb.velocity = transform.forward * -travel; // moves object
            _distance -= travel; // update distance;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
