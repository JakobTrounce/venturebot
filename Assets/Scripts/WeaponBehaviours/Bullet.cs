﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Ammo
{
    public bool debugEnabled = false;

    protected override void Awake()
    {
        base.Awake();
    }

    // Update is called once per frame
    void Update()
    {
        if(!IsInitialized)
        {
            return;
        }

        // Maybe do this logic in a function
        rb.velocity = direction * bulletSpeed;

        //if (debugEnabled)
        //{
            Debug.DrawRay(transform.position, direction * 10, Color.cyan, 0);
        //}
    }



    private void OnBecameInvisible()
    {
        RecycleObject();
    }

    protected override void RecycleObject()
    {
        // anything specific to the bullet.
        base.RecycleObject();
    }
}
