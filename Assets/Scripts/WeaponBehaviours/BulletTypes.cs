﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType
{
    bullet,
    missile,
    boomerang,
    bomb,
    laser
}
