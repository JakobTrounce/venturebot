﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Gun : MonoBehaviour {
    public GameObject player;
    public GameObject ammoPrefab;
    public Transform spawnPosition;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    void Shoot()
    {
        var bullet = (GameObject)Instantiate(ammoPrefab, spawnPosition.position, Quaternion.LookRotation(player.transform.forward));
        bullet.tag = ("PlayerBullet");
    }
}
