﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunShell : MonoBehaviour {

    [SerializeField]
    private GameObject shotParticle;
    private GameObject[] spawnedshots;

    [Tooltip("Number of shots fired from shell")]
    public int shots;

    [Tooltip("Shotgun spread from muzzle in degrees")]
    [Range(0f, 60f)]
    public float spread;

    [Tooltip("Shot velocity range")]
    [Range(10f, 50f)]
    public float shotVelocity;

    //[Tooltip("Damage multiplier based on individual ")]
    //[Range(0.05f, 1f)]
    //public float damage;


	void Start () {
        InstantiateShots();

    }
	
	void InstantiateShots()
    {
        // gather resources
        spawnedshots = new GameObject[shots];

         // set full range from bottom to top
        float range = 2 * transform.localScale.y;
        // set bracket sizing for spaces between shots
        float adjustor = range / shots;

        for (int i = 0; i < shots; i++)
        {

            transform.position = /*transform.localPosition + */new Vector3(0,-transform.localScale.y + (i*adjustor), 0 );
            Quaternion rotation = Quaternion.AngleAxis((-spread* .5f) * transform.position.y, transform.right);

            spawnedshots[i] = Instantiate(shotParticle, transform.position, rotation);

            // adjust scale and bulletSpeed
            spawnedshots[i].transform.localScale *= transform.localScale.y * .5f;
            spawnedshots[i].GetComponent<Bullet>().bulletSpeed = shotVelocity * Random.Range(0.9f, 1.1f);
        }
    }
}
