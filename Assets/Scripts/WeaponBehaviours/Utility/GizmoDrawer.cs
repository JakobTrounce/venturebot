﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoDrawer : MonoBehaviour
{
    public enum GizmoType
    {
        Sphere,
        Cube,
        WireCube,
        WireSphere,
    }
    public GizmoType type;
    public float size;
    public Color colour;

    private void OnDrawGizmos()
    {
        Gizmos.color = colour;

        switch (type)
        {
            case GizmoType.Cube:
                Gizmos.DrawCube(transform.position, new Vector3(size, size, size));
                break;
            case GizmoType.Sphere:
                Gizmos.DrawSphere(transform.position, size);
                break;
            case GizmoType.WireCube:
                Gizmos.DrawWireCube(transform.position, new Vector3(size, size, size));
                break;
            case GizmoType.WireSphere:
                Gizmos.DrawWireSphere(transform.position, size);
                break;
        }
    }
}
