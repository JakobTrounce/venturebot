﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadLastLevel : MonoBehaviour {

    public LevelManager levelManager;
    public string previousLevel;

	// Use this for initialization
	void Start () {

        previousLevel = levelManager.currentLevelName;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void LoadLastScene()
    {
        SceneManager.LoadScene(previousLevel);
    }

    public void LoadStartMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }
}
