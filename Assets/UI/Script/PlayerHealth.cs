﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public RectTransform healthBar;
    public PlayerController playerScript;

    private Camera _mainCamera;

    private void Start()
    {
        _mainCamera = Camera.main;
    }

    void Update()
    {
        transform.LookAt(transform.position + _mainCamera.transform.rotation * Vector3.forward, _mainCamera.transform.rotation * Vector3.up);
        healthBar.sizeDelta = new Vector2(playerScript.PlayerHealth() * 16, healthBar.sizeDelta.y);
    }
}
