﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopMenuManagerScript : MonoBehaviour
{

    public GameObject weaponSocket;
    public GameObject[] weaponPrefabs;



    //ToReferenceInOtherScripts
    public GameObject currentPrimarySelectedWeapon;
    public GameObject currentSecondarySelectedWeapon;
    public GameObject currentWeapon;


    private bool isSWitchingWeapon1;
    private bool isSWitchingWeapon2;

    //Final
    public GameObject[] primaryWeaponPrefabs;
    public GameObject[] secondaryWeaponPrefabs;





    //Menus
    public GameObject primaryWeaponMenu;
    public GameObject secondaryWeaponMenu;
    /*public GameObject primaryWeaponText;
     public GameObject secondaryWeaponText;*/

    // Mesh OnPlayer
    public GameObject[] weaponMeshPrefab;


    void Start()
    {
        //print(currentWeapon.name);
        primaryWeaponMenu.SetActive(false);
        reintializeWeaponMesh();




    }

    public void selectWeapon1()
    {
        currentPrimarySelectedWeapon = weaponPrefabs[0];


        if (isSWitchingWeapon1)
        {

            currentPrimarySelectedWeapon = primaryWeaponPrefabs[0];
            reinitializePrimaryWeaponPrefabs();
            primaryWeaponPrefabs[0].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[0].SetActive(true);
        }
        else if (isSWitchingWeapon2)
        {

            currentSecondarySelectedWeapon = secondaryWeaponPrefabs[0];
            reinitializeSecondaryWeaponPrefabs();
            secondaryWeaponPrefabs[0].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[0].SetActive(true);
        }


    }
    public void selectWeapon2()
    {
        currentPrimarySelectedWeapon = weaponPrefabs[1];



        if (isSWitchingWeapon1)
        {
            currentPrimarySelectedWeapon = primaryWeaponPrefabs[1];
            reinitializePrimaryWeaponPrefabs();
            primaryWeaponPrefabs[1].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[1].SetActive(true);
        }
        else if (isSWitchingWeapon2)
        {
            currentSecondarySelectedWeapon = secondaryWeaponPrefabs[1];
            reinitializeSecondaryWeaponPrefabs();
            secondaryWeaponPrefabs[1].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[1].SetActive(true);
        }



    }
    public void selectWeapon3()
    {

        currentPrimarySelectedWeapon = weaponPrefabs[2];


        if (isSWitchingWeapon1)
        {
            currentPrimarySelectedWeapon = primaryWeaponPrefabs[2];
            reinitializePrimaryWeaponPrefabs();
            primaryWeaponPrefabs[2].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[2].SetActive(true);
        }
        else if (isSWitchingWeapon2)
        {
            currentSecondarySelectedWeapon = secondaryWeaponPrefabs[2];
            reinitializeSecondaryWeaponPrefabs();
            secondaryWeaponPrefabs[2].SetActive(true);
            reintializeWeaponMesh();
            weaponMeshPrefab[2].SetActive(true);
        }



    }
    public void SwitchMainWeapon()
    {
        isSWitchingWeapon1 = true;
        isSWitchingWeapon2 = false;
        secondaryWeaponMenu.SetActive(false);
        primaryWeaponMenu.SetActive(true);


    }
    public void SwitchSecondWeapon()
    {
        isSWitchingWeapon2 = true;
        isSWitchingWeapon1 = false;
        primaryWeaponMenu.SetActive(false);
        secondaryWeaponMenu.SetActive(true);

    }
    // Update is called once per frame

    private void reinitializePrimaryWeaponPrefabs()
    {
        for (int i = 0; i < primaryWeaponPrefabs.Length; i++)
        {
            primaryWeaponPrefabs[i].SetActive(false);
        }
    }
    private void reinitializeSecondaryWeaponPrefabs()
    {
        for (int i = 0; i < primaryWeaponPrefabs.Length; i++)
        {
            secondaryWeaponPrefabs[i].SetActive(false);
        }
    }
    private void reintializeWeaponMesh()
    {
        for (int i = 0; i < primaryWeaponPrefabs.Length; i++)
        {
            weaponMeshPrefab[i].SetActive(false);
        }
    }
    public void BackToTheOverworld()
    {
        SceneManager.LoadScene("Overworld");
    }


    void Update()
    {

    }


}
