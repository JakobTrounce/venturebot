﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDamageBlock : MonoBehaviour
{
    private PlayerController playerController;
	public int damage;

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			playerController = collision.gameObject.GetComponent<PlayerController>();
			playerController.TakeDamage(damage);
		}
	}
	/*
	public void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			print("Bitch");
			playerController = collision.gameObject.GetComponent<PlayerController>();
			playerController.TakeDamage(damage);
		}
	}
	*/

}
