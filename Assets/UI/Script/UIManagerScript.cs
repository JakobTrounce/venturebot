﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;



public class UIManagerScript : MonoBehaviour {

    public GameObject pauseCanvas;
    public GameObject optionsCanvas;
    public GameObject mainHud;
    public GameObject gameCamera;
    public GameObject canvasCamera;
    public GameObject allGameMenus;
    public Light directionalLight;

    private bool optionsMenuIsOn = false;
    private bool pressedResume = false;

    public GameObject pauseBackground;
    public GameObject optionBackground;

    //Settings
    public GameObject audioSettings;
    public GameObject videoSettings;
    public GameObject gameplaySettings;


    //VideoSettings
    public float rgbValue;

    // Main Hud Weapons
    public Sprite weapon1Sprite;
    public Sprite weapon2Sprite;

    public Image primaryWeaponSlot;
    public Image secondaryWeaponSlot;
    private bool switchedWeapons;

    // Weapons
    public GameObject[] weapons;



    // Use this for initialization
    void Start () {

        mainHud.SetActive(true);
        pauseCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        switchedWeapons = false;
        canvasCamera.SetActive(false);
  

    }
	
	// Update is called once per frame
	void Update ()
    {
       

        WeaponSwap();
        if (optionsMenuIsOn == false)
        {
            if (Input.GetKeyDown("escape"))
            {
                

                if (Time.timeScale == 1)
                {
                    allGameMenus.SetActive(true);
                    gameCamera.SetActive(false);
                    canvasCamera.SetActive(true);
                    mainHud.SetActive(false);
                    pauseCanvas.SetActive(true);
                    Time.timeScale = 0;

                }

                else
                {
                    gameCamera.SetActive(true);
                    canvasCamera.SetActive(false);

                    Time.timeScale = 1;
                    pauseCanvas.SetActive(false);
                    mainHud.SetActive(true);
                }

            }
            if (optionsMenuIsOn == true)
            {
               
                if (Input.GetKeyDown("escape"))
                {
                    
                    optionsCanvas.SetActive(false);
                    optionsMenuIsOn = false;
                }
                

            }
        }
	}

    public void ResumeGameButton()
    {
        gameCamera.SetActive(true);
        canvasCamera.SetActive(false);
        allGameMenus.SetActive(false);
        pressedResume = true;
        Time.timeScale = 1;
        pauseCanvas.SetActive(false);
    }
    public void MainMenuButton()
    {
        SceneManager.LoadScene("StartMenu");
    }
    public void backButton()
    {
        optionsCanvas.SetActive(false);
        optionsMenuIsOn = false;
        pauseCanvas.SetActive(true);
    }
     


    // Options Menu
    public void OptionsCanvas()
    {
        optionsMenuIsOn = true;
        pauseCanvas.SetActive(false);
        optionsCanvas.SetActive(true);

        
    }
    public void AudioSettings()
    {
        audioSettings.SetActive(true);
        videoSettings.SetActive(false);
        gameplaySettings.SetActive(false);
    }
    public void VideoSettings()
    {
        videoSettings.SetActive(true);
        audioSettings.SetActive(false);
        gameplaySettings.SetActive(false);
    }
    public void GameplaySettings()
    {
        gameplaySettings.SetActive(true);
        videoSettings.SetActive(false);
        audioSettings.SetActive(false);
    }

    public void WeaponSwap()
    {

        if (Input.GetKeyDown("q") & switchedWeapons == false)
        {
            
            primaryWeaponSlot.sprite = weapon2Sprite;
            secondaryWeaponSlot.sprite = weapon1Sprite;
            switchedWeapons = true;
        }
        else if (Input.GetKeyDown("q") & switchedWeapons == true)
        {
            primaryWeaponSlot.sprite = weapon1Sprite;
            secondaryWeaponSlot.sprite = weapon2Sprite;
            switchedWeapons = false;
        }
    }




}
