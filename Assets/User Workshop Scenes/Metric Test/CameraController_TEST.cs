﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_TEST : MonoBehaviour
{

    // public
    [SerializeField]
    private GameObject player;

    public float trailTimer;
    public float cameraSpeed;
    public float offset;
    public float CameraZPos;


    // private
    private float timerSet;
    private float MoveTimer;
    private Vector3 lastPlayerPos;
    private Vector3 trailDistance;
    private Vector3 CameraPos;


	// Use this for initialization
	void Start ()
    {
        CameraPos = transform.position - player.transform.position;
        timerSet = trailTimer;
        MoveTimer = trailTimer;
        trailDistance = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(isPlayerMoving(lastPlayerPos)&& trailTimer > 0f)
        {
            if(player.transform.position.x > lastPlayerPos.x)
            {
                trailDistance.x += Time.deltaTime*cameraSpeed;
            }
            else if(player.transform.position.x < lastPlayerPos.x)
            {
                trailDistance.x -= Time.deltaTime*cameraSpeed;
            }
            if(player.transform.position.y > lastPlayerPos.y)
            {
                trailDistance.y += Time.deltaTime*cameraSpeed;
            }
            else if(player.transform.position.y < lastPlayerPos.y)
            {
                trailDistance.y -= Time.deltaTime*cameraSpeed; 
            }

            trailTimer -= Time.deltaTime*cameraSpeed;
        }
        else if (!isPlayerMoving(lastPlayerPos) && trailTimer < timerSet)
        {
            if(trailDistance.x > 0)
                trailDistance.x -= Time.deltaTime*cameraSpeed;
            if (trailDistance.x < 0)
                trailDistance.x += Time.deltaTime*cameraSpeed;
            if (trailDistance.y > 0)
                trailDistance.y -= Time.deltaTime*cameraSpeed;
            if (trailDistance.y < 0)
                trailDistance.y += Time.deltaTime*cameraSpeed;

            trailTimer += Time.deltaTime;
        }
        transform.position = new Vector3((player.transform.position.x - trailDistance.x), player.transform.position.y + offset - trailDistance.y, CameraZPos);
        lastPlayerPos = player.transform.position;
	}

    private void FixedUpdate()
    {

    }




    bool isPlayerMoving(Vector3 lastPos)
    {
        if (player.transform.position == lastPos)
        {
            return false;
        }
        else
            return true;
    }
}
