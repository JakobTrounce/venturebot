﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    [SerializeField]
    PlayerStats stats;
    public bool useDefault;
    float speed = 9;
    float walkAcceleration = 75;
    float airAcceleration = 30;
    float groundDeceleration = 70;
    float jumpHeight = 4;
    public float damageDelay;
    public float _gravity;
    DataLoader _dataLoader;
    Camera _cam;
    float playerHealth;
    int maxjumps = 2;
    int numberofjumps = 0;
    private float damageTimer = 0f;

    // movement abilities
    public bool hasDoubleJump = true;
    public bool hasDash = false;
    public bool hasDoubleDash = false;
    ////////////////
   
    private BoxCollider2D boxCollider;

    [SerializeField]
    private GameObject [] weapons;
	[SerializeField]
	int maxDash = 0;
	[SerializeField]
	float cooldownDash;
	[SerializeField]
	Vector2 DragDash;
	[SerializeField]
	float dashTimer;
	[SerializeField]
	float dashDelay;
	[SerializeField]
	float dashDistance;

	private bool canDash;
	private Vector2 velocity;
    private ObjectPoolManager objectPoolManager;
    private LevelManager _levelManager;
    private bool grounded;

    private bool canInputMove = true;

    // Animating the player
    public Animator playerAnimator;
   
    float delayTimer = 0.1f;
    bool isFalling;
    bool stopJump = false;
    float checkNewV;
    bool saveFallValue = false;
    bool IsTakingDmg;
    bool doubleJumpCheck;
    bool stopDoubleJump;
    float jumpTimer = 1f;
    bool deathAnim = false;
    float deathDelay = 1f;
    float dashAnimTimer = 0.2f;
    bool dashAnim;




    private void Awake()
    {
       
        if(!useDefault)
        {
            
            speed = stats.moveSpd;
            walkAcceleration = stats.walkAcceleration;
            airAcceleration = stats.airAcceleration;
            groundDeceleration = stats.groundDeceleration;
            jumpHeight = stats.JumpHeight;
            print(speed);
        }
        boxCollider = GetComponent<BoxCollider2D>();
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update ()
    {
        if (canInputMove)
        {
			

			float moveInput = Input.GetAxisRaw("Horizontal");
            
            // TurnLeft Anim
            if (moveInput == 1)
            {
                playerAnimator.SetBool("IsRunning", true);
			
			}
           
            if (moveInput == -1)
            {
                playerAnimator.SetBool("IsRunning", true);
			

			}

			if(transform.right.x < 0f)
			{
				moveInput *= -1;
			}
			if (moveInput == 0)
            {
                    playerAnimator.SetBool("IsRunning", false);
                
            }

            if (grounded)
            {
             
                velocity.y = 0;
                playerAnimator.SetBool("IsFalling", false);


                if (Input.GetButtonDown("Jump"))
                {
                    //  Anim Jum
                    playerAnimator.SetBool("Jump", true);
                    
                    //ZenBe

                    velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
                    velocity.y += Physics2D.gravity.y * Time.deltaTime;
                    numberofjumps++;
                    stopJump = true;
                }

                //  Anim Jump
                if (stopJump == true)
                {
                    delayTimer -= Time.deltaTime;
                    if (delayTimer <= 0)
                    {
                        print("StopJump");
                        playerAnimator.SetBool("Jump", false);
                        delayTimer = 0.1f;
                        stopJump = false;
                    }
                }
                //ZenBe
            }
            // Anim TakeDmg
            if (IsTakingDmg == true)
            {
                delayTimer -= Time.deltaTime;
                if (delayTimer <= 0)
                {
                    playerAnimator.SetBool("TakeDamage", false);
                    IsTakingDmg = false;
                    delayTimer = 0.1f;
                }
            }
            //Anim Fall
            if (!grounded)
            {
				// Anim Check
                if (velocity.y < 0)
                {
                    playerAnimator.SetBool("IsFalling", true);
                   
                }
                
            }
            //ZenBe
            // Anim Dash
            if (dashAnim == true)
            {
                dashAnimTimer -= Time.deltaTime;
                if (dashAnimTimer <= 0)
                {
                    playerAnimator.SetBool("IsDashing", false);
                    dashAnim = false;
                    dashAnimTimer = 0.2f;
                }
            }

            if (!grounded && Input.GetButtonDown("Jump") && hasDoubleJump)
            {
                if (numberofjumps < maxjumps)
                {
                    playerAnimator.SetBool("DoubleJump", true);
                    playerAnimator.SetBool("Jump", false);
                    playerAnimator.SetBool("IsRunning", false);
                    stopDoubleJump = true;
                    stopJump = true;
                    saveFallValue = true;
                    velocity.y = 0;
                    velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
                    velocity.y += Physics2D.gravity.y * Time.deltaTime;
                    numberofjumps++;
                   
                }
                else
                {
                    playerAnimator.SetBool("Jump", false);
                }
            }

            if (Input.GetButtonDown("Dash") && canDash )
            {
                print("Inside dash function.");
                // Anim Dash
                playerAnimator.SetBool("IsDashing", true);
                dashAnim = true;
                // ZenBe
                Dash(dashDistance * walkAcceleration);
               
            }

            // Dash check        -------------------------------------------------------------------
            if (maxDash > 1 && canDash)
            {
                dashTimer = cooldownDash;
                canDash = false;
            }

            if (dashTimer <= 0f && !canDash)
            {
                canDash = true;
                maxDash = 0;
                cooldownDash = 0;
            }

            if (dashTimer <= 0f && maxDash == 1)
            {
                cooldownDash = 0;
                maxDash = 0;
            }
            // End of dash check -------------------------------------------------------------------

            if (Input.GetButtonDown("Fire"))
            {
                foreach (var gun in weapons)
                {
                    if (gun.activeInHierarchy)
                    {
                        Weapon w = gun.GetComponent<Weapon>();
                        w.Fire(stats);
                    }
                }

            }

            float acceleration = grounded ? walkAcceleration : airAcceleration;
            float deceleration = grounded ? groundDeceleration : 0;
          
            if (moveInput != 0)
            {
                velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, walkAcceleration * Time.deltaTime);
               
				// Anim check
                if (grounded)
                {
                    playerAnimator.SetBool("IsFalling", false);
                }

            }
            else
            {
                velocity.x = Mathf.MoveTowards(velocity.x, 0, groundDeceleration * Time.deltaTime);
            }
            velocity.y += Physics2D.gravity.y * _gravity * Time.deltaTime;
            transform.Translate(velocity * Time.deltaTime);

            Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);
            grounded = false;
            foreach (Collider2D hit in hits)
            {
                if (hit == boxCollider || hit.isTrigger)
                    continue;
                if (hit.gameObject.tag == "Platform" && velocity.y > 0)
                    continue;
                if(hit.gameObject.CompareTag("Enemy")&& damageTimer >= damageDelay)
                {
                    Enemy e = hit.gameObject.GetComponent<Enemy>();
                    TakeDamage(e.stats.bulletDmg);
                    damageTimer = 0f;

                }

                ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

                if (colliderDistance.isOverlapped)
                {
					if(transform.right.x < 0f)
					{
						transform.Translate(-(colliderDistance.pointA - colliderDistance.pointB).x,0,0);
						transform.Translate(0,(colliderDistance.pointA - colliderDistance.pointB).y,0);
					}
					else
					{
						transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
					}
					if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
                    {
                        grounded = true;
                        numberofjumps = 0;
                    }
                    if (Vector2.Angle(colliderDistance.normal, Vector2.down) < 20 && velocity.y > 0)
                    {
                        velocity.y = 0f;
                    }
                }

            }
            dashTimer -= Time.deltaTime;
            damageTimer += Time.deltaTime;
        }

		if(playerHealth <= 0f)
		{
			var levelManagerObj = GameObject.FindGameObjectWithTag("LevelManager");
			LevelManager lm = levelManagerObj.GetComponent<LevelManager>();
			this.gameObject.SetActive(false);
			this.playerHealth = stats.PlayerHealth;
 
		}
	}

    public void Initialize()
    {
        playerHealth = stats.PlayerHealth;
        _cam = Camera.main;

        objectPoolManager = ServiceLocator.Get<ObjectPoolManager>();
        if (objectPoolManager == null)
        {
            Debug.LogError("Couldn't Find the ObjectPool");
        }

        _dataLoader = ServiceLocator.Get<DataLoader>();
        if (_dataLoader == null)
        {
            Debug.LogError("Couldn't Find the DataLoader");
        }

        _levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        if(_levelManager == null)
        {
            Debug.LogError("level manager not found");
        }

        if (SceneManager.GetActiveScene().name == "Denial")
        {
            hasDoubleJump = true;
        }

    }

    public float PlayerHealth()
    {
        return playerHealth;
    }

    public PlayerStats Stats()
    {
        return stats;
    }

    public void TakeDamage(float dmg)
    {
		
        playerHealth -= dmg;
        playerAnimator.SetBool("TakeDamage", true);
        IsTakingDmg = true;

    }

    public void ApplySpringTrap(Vector2 force, springTrap.ForceMode mode)
    {
        // Hack to avoid the y velocity being cancelled by a previous grounding
        // occurance.
        // - Erik Ross
        grounded = false;

        switch (mode)
        {
            case springTrap.ForceMode.Additive:
                velocity += force;
                break;
            case springTrap.ForceMode.Instant:
                transform.Translate(force);
                break;
            default:
                Debug.LogWarning(string.Format("Unknown SpringTrap ForceMode: {0} Using Additive by default", mode.ToString()));
                velocity += force;
                break;
        }
    }

	void Dash(float distance)
	{
		print("Inside dash function.");
		++maxDash;
		
		if(transform.right.x == 1f)
		velocity += Vector2.Scale(transform.right, dashDistance * new Vector2((Mathf.Log(1f / (Time.deltaTime * DragDash.x + 1)) / -Time.deltaTime), 0));
		else
		velocity -= Vector2.Scale(transform.right, dashDistance * new Vector2((Mathf.Log(1f / (Time.deltaTime * DragDash.x + 1)) / -Time.deltaTime), 0));



		cooldownDash += 5f;
		dashTimer = dashDelay;
	}
    //Extra Animation Functions
    public void DeathAnim()
    {
        if (deathAnim == false)
        {

            playerAnimator.SetBool("Death", true);
            deathAnim = true;

        }
        else
        {
            playerAnimator.SetBool("TakeDamage", false);
            playerAnimator.SetBool("Death", false);
        }


    }

    public bool DoubleJump
    {
        get { return hasDoubleJump; }
        set { hasDoubleJump = value; }
    }
    public bool DoubleDash
    {
        get { return hasDoubleDash; }
        set { hasDoubleDash = value; }
    }
    public bool HasDash
    {
        get { return hasDash; }
        set { hasDash= value; }
    }


}
