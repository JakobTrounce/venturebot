﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform SpawnPos;
    private Camera cam;
    private ObjectPoolManager objectPoolManager;
    private GameObject _playerObj;


    private void Awake()
    {
        cam = Camera.main;
        if(cam == null)
        {
            Debug.LogError("Camera not found");
        }
        objectPoolManager = ServiceLocator.Get<ObjectPoolManager>();
        if (objectPoolManager == null)
        {
            Debug.LogError("Couldn't Find the ObjectPool");
        }
        _playerObj = GameObject.FindGameObjectWithTag("Player");
        if(_playerObj == null)
        {
            Debug.LogError("Player is null");
        }
    }
    private void Update()
    {
        if(_playerObj == null)
        {
            _playerObj = GameObject.FindGameObjectWithTag("Player");
        }
        Vector2 target = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
	
		//else if (Vector2.Angle(_playerObj.transform.Position2D(), target) < 0 && transform.right.x < 0f)
		//{
		//	_playerObj.transform.Rotate(0, 180, 0);
		//}
		Vector2 objPos = cam.WorldToScreenPoint(transform.Position2D());
        target.x = target.x - objPos.x;
        target.y = target.y - objPos.y;
	
		float angle = Mathf.Atan2(target.y, target.x)*Mathf.Rad2Deg;
        float UpAngle =  Vector2.Angle(_playerObj.transform.up, target);

        if (UpAngle < 30 && UpAngle >= 0 || UpAngle <= 180 && UpAngle > 150)
        {

        }
        else if ( target.x < _playerObj.transform.Position2D().x && Input.GetMouseButtonDown(0) && _playerObj.transform.right.x > 0)
		{
			_playerObj.transform.Rotate(0, 180, 0);
		}
        else if (target.x > _playerObj.transform.Position2D().x && Input.GetMouseButtonDown(0) && _playerObj.transform.right.x < 0)
        {
            _playerObj.transform.Rotate(0, 180, 0);
        }
      


        //shooting clamped to 180, player manually flips direction
        if (_playerObj.transform.right.x > 0f)
        {
            angle = Mathf.Clamp(angle, -90f, 90f);
        }
        else if (_playerObj.transform.right.x < 0f)
        {

            if (angle >= 90f)
            {
                angle = Mathf.Clamp(angle, 90f, 180f);
            }
            else if (angle <= -90f)
            {
                angle = Mathf.Clamp(angle, -180f, -90f);
            }
            else
            {
                angle = -Mathf.Clamp(angle, -180f, -180f);
            }

        }



        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        Debug.DrawRay(transform.Position2D(), target, Color.blue);
    }
    public void Fire(PlayerStats stats)
    {
        Vector2 mousePosition = new Vector2(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y);
        var bullet = objectPoolManager.GetObjectFromPool("Bullet");
		if(bullet == null)
		{
			return;
		}
        bullet.SetActive(true);
        bullet.transform.position = SpawnPos.position;
        bullet.transform.rotation = transform.rotation;
		bullet.tag = "PlayerBullet";
        Ammo a = bullet.GetComponent<Ammo>();
        a.Initialize(stats.damage, transform.right);
    }
}
