﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/MagneticFollow")]
public class MagneticFollowAction : AIAction
{


    public override void Act(Statecontroller controller)
    {
		MagneticFollow(controller);
    }

	MagneticFollow(Statecontroller controller)
	{
		Vector2 direction;
		direction = (controller.GetPlayer().position - Controller.transform.position).normalized;//p2 - p1
		Controller.GetRB2D.AddForce(direction);
	}
}
