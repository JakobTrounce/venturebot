﻿using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private GameObject[] ammoPrefab;
    [SerializeField] private GameObject respawn;
    public PlayerStats PlayerStats;
    private Vector3 mVelocity;
    [SerializeField] private float dashTimer = 0f;
    public float dashMultiplier;
    private ObjectPoolManager objectPoolManager;

    public UnityEngine.CharacterController mController;
    private int loadout;
    private int maxJumps;
    public float gravity;
    public float jumpHeight;
    //[HideInInspector] bool doubleJumpCheck = false;

    public float dashDistance;
    public float dashDelay;
    private bool canDash;
    [SerializeField] private int maxDash;
    [SerializeField] private float cooldownDash;

    public float playerHealth;
    public Transform spawnPosition;

    public GameObject[] Left_Weapons;
    public GameObject[] Right_Weapons;

    private Camera _cam;
    private DataLoader _dataLoader;

    private bool canInputMove = true;

    // Animating the player
    public Animator playerAnimator;
    public float maxTimer;
    float delayTimer = 0.1f;
    bool isfalling;
    bool stopJump = false;
    float checkNewV;
    bool saveFallValue;
    bool TurnLeft;
    bool TurnRight;
    bool IsTakingDmg;
    bool doubleJumpCheck;
    bool stopDoubleJump;
    float jumpTimer = 1f;
    bool deathAnim = false;
    float deathDelay = 1f;
    float dashAnimTimer = 0.2f;
    bool dashAnim;



    // Use this for initialization
    public void Initialize()
    {
        InitializeWeapons();

        mController = GetComponent<UnityEngine.CharacterController>();

        if (mController == null)
        {
            Debug.LogError("Player is missing a character controller component");
        }

        playerHealth = PlayerStats.PlayerHealth;
        print(playerHealth);
        print("Init.");

        _cam = Camera.main;

        objectPoolManager = ServiceLocator.Get<ObjectPoolManager>();
        if (objectPoolManager == null)
        {
            Debug.LogError("Couldn't Find the ObjectPool");
        }

        _dataLoader = ServiceLocator.Get<DataLoader>();
        if (_dataLoader == null)
        {
            Debug.LogError("Couldn't Find the DataLoader");
        }

        IDataSource dataSource;
        if (_dataLoader.LoadedDataSources.TryGetValue("JsonTestFile", out dataSource))
        {
            if (dataSource is JsonDataSource)
            {
                JsonDataSource jsonDataSource = dataSource as JsonDataSource;
                string message = jsonDataSource.DataDictionary["name"] as string;
                Debug.Log("<color=lime>Player Name is : </color>" + message);
            }
        }
    }

    private void InitializeWeapons()
    {
        loadout = 0;
        Left_Weapons[1].SetActive(true);
        Left_Weapons[0].SetActive(false);
        Right_Weapons[0].SetActive(true);
        Right_Weapons[1].SetActive(false);
    }

    // Update is called once per frame

    void Update()
    {

        if (canInputMove)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
            Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            if (move != Vector3.zero)
            {
                transform.forward = move;

            }
            if (move != Vector3.zero && mController.isGrounded)
            {
                    playerAnimator.SetBool("TurnRight", false);
                    playerAnimator.SetBool("TurnLeft", false);
                    playerAnimator.SetBool("IsRunning", true);
            }
            //Idle
            if (move == Vector3.zero)
            {
                playerAnimator.SetBool("IsRunning", false);
            }
            if (move.x > 0 && Input.GetButtonDown("Horizontal"))
            {
                playerAnimator.SetBool("IsRunning", false);
                playerAnimator.SetBool("TurnLeft", true);
               
            }

            if (move.x < 0 && Input.GetButtonDown("Horizontal"))
            {
                playerAnimator.SetBool("IsRunning", false);
                playerAnimator.SetBool("TurnRight", true);
            }
       
            mController.Move(move * PlayerStats.moveSpd * Time.deltaTime);
            mVelocity.y -= gravity * Time.deltaTime;

            mController.Move(mVelocity * Time.deltaTime);

            if (Input.GetButtonDown("Dash") && canDash)
            {

                Dash(dashDistance * PlayerStats.moveSpd);
                playerAnimator.SetBool("IsDashing", true);
                dashAnim = true;

            }
            if (dashAnim == true)
            {
                dashAnimTimer -= Time.deltaTime;
                if (dashAnimTimer <= 0)
                {
                    playerAnimator.SetBool("IsDashing", false);
                    dashAnim = false;
                    dashAnimTimer = 0.2f;
                }
            }


            if (Input.GetButtonDown("Jump") && mController.isGrounded)
            {
                playerAnimator.SetBool("Jump", true);

                stopJump = true;
                Jump();
                saveFallValue = true;
                if (stopJump == true)
                {
                    delayTimer -= Time.deltaTime;
                    if (delayTimer <= 0)
                    {
                        playerAnimator.SetBool("Jump", false);
                        delayTimer = maxTimer;
                        stopJump = false;
                    }

                }
            }
            //Anim Damage
            if (IsTakingDmg == true)
            {
                delayTimer -= Time.deltaTime;
                if (delayTimer <= 0)
                {
                    playerAnimator.SetBool("TakeDamage", false);
                    print("FinishedTakingDamage");
                    IsTakingDmg = false;
                    delayTimer = maxTimer;
                }
            }
            if ((!mController.isGrounded && Input.GetButtonDown("Jump")))
            {
                stopJump = false;
                if (maxJumps < 1)
                {
                    playerAnimator.SetBool("DoubleJump", true);
                    playerAnimator.SetBool("Jump", false);
                    playerAnimator.SetBool("IsRunning", false);
                    stopDoubleJump = true;
                    stopJump = true;
                    mVelocity.y = 0;
                    Jump();
                    saveFallValue = true;

                }

                else
                {
                    playerAnimator.SetBool("Jump", false);
                }

            }
            // Anim Stop Double Jump
            if (stopDoubleJump)
            {
                jumpTimer -= Time.deltaTime;
                if (jumpTimer <= 0)
                {
                    playerAnimator.SetBool("DoubleJump", false);
                    stopDoubleJump = false;
                    jumpTimer = 0.5f;
                }
            }

            if (mController.isGrounded)
            {
                maxJumps = 0;
                if (stopJump == true)
                {
                    delayTimer -= Time.deltaTime;
                    if (delayTimer <= 0)
                    {
                        playerAnimator.SetBool("Jump", false);
                        playerAnimator.SetBool("DoubleJump", false);
                        delayTimer = maxTimer;
                        stopJump = false;
                    }
                }
                
            }
            if (!mController.isGrounded)
            {
                if (mVelocity.y < 0)
                {
                    playerAnimator.SetBool("IsFalling", true);
                }

            }
            // Dash check        -------------------------------------------------------------------
            if (maxDash > 1 && canDash)
            {
                dashTimer = cooldownDash;
                canDash = false;
            }

            if (dashTimer <= 0f && !canDash)
            {
                canDash = true;
                maxDash = 0;
                cooldownDash = 0;
            }

            if (dashTimer <= 0f && maxDash == 1)
            {
                cooldownDash = 0;
                maxDash = 0;
            }
            // End of dash check -------------------------------------------------------------------

            // Ceiling check
            if ((mController.collisionFlags & CollisionFlags.Above) != 0)
            {
                if (mVelocity.y > 0)
                {
                    mVelocity.y = 0f;

                }
            }

            // Ground check
            if (mController.isGrounded && mVelocity.y < 0)
            {

                mVelocity.y = 0f;
                // Anim Fall Trigger
                playerAnimator.SetBool("IsFalling", false);

            }

            // Weapon switch
            if (Input.GetKeyDown(KeyCode.Q))
            {
                ModelSwitch();
                Debug.Log("Weapon loadout switched");
            }

            // Fire the player gun
            if (Input.GetButtonDown("Fire1"))
            {
                foreach (var gun in Right_Weapons)
                {
                    if (gun.activeInHierarchy == true)
                    {
                        var bullet = objectPoolManager.GetObjectFromPool("Bullet");
                        bullet.SetActive(true);
                        bullet.transform.position = spawnPosition.position;
                        bullet.transform.rotation = transform.rotation;
                        Ammo a = bullet.GetComponent<Ammo>();
                        a.Initialize(PlayerStats.damage, transform.forward);
                    }
                }
            }

            Debug.DrawRay(transform.position, transform.forward * 10, Color.green, 0); // Ray showing the forward vector of the player
            mVelocity.x /= 1 + PlayerStats.Drag.x * Time.deltaTime;
            dashTimer -= Time.deltaTime;
            // HE DIES
        }
        if (playerHealth <= 0)
        {
            canInputMove = false;
            DeathAnim();
            print(deathAnim);
            
            deathDelay -= Time.deltaTime;
            if (deathDelay <= 0)
            {
                var levelManagerObj = GameObject.FindGameObjectWithTag("LevelManager");
                LevelManager lm = levelManagerObj.GetComponent<LevelManager>();
                lm.Initialize();
                this.gameObject.SetActive(false);
                this.playerHealth = PlayerStats.PlayerHealth;
                deathDelay = 1f;
            }
        }
    }

    private void LoadSavedData()
    {
        float savedHealth = PlayerPrefs.GetFloat(GameDefs.PlayerSaveInfo.PLAYER_HEALTH_KEY);
    }

    private void SaveData()
    {
        PlayerPrefs.SetFloat(GameDefs.PlayerSaveInfo.PLAYER_HEALTH_KEY, playerHealth);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SpringTrap"))
        {
            Debug.Log("Spring");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Enemy"))
        {
            Enemy e = other.gameObject.GetComponent<Enemy>();
            if (e == null)
            {
                return;
            }
            else
            {
                this.TakeDamage(e.stats.bulletDmg);
            }
        }
    }

    void ModelSwitch()
    {
        if (loadout == 0)
        {
            this.loadout = 1;
            Left_Weapons[1].SetActive(false);
            Left_Weapons[0].SetActive(true);

            Right_Weapons[0].SetActive(false);
            Right_Weapons[1].SetActive(true);
        }
        else if (loadout == 1)
        {
            this.loadout = 0;
            Left_Weapons[1].SetActive(true);
            Left_Weapons[0].SetActive(false);

            Right_Weapons[0].SetActive(true);
            Right_Weapons[1].SetActive(false);
        }
    }

    void Dash(float distance)
    {
        ++maxDash;
        mVelocity += Vector3.Scale(transform.forward, dashDistance * new Vector3((Mathf.Log(1f / (Time.deltaTime * PlayerStats.Drag.x + 1)) / -Time.deltaTime), 0, 0));
        cooldownDash += 5f;
        dashTimer = dashDelay;
    }

    public void ApplySpringTrap(Vector3 force, springTrap.ForceMode mode)
    {
        switch (mode)
        {
            case springTrap.ForceMode.Additive:
                mVelocity += force;
                break;
            case springTrap.ForceMode.Instant:
                mController.Move(force);
                break;
            default:
                Debug.LogWarning(string.Format("Unknown SpringTrap ForceMode: {0} Using Additive by default", mode.ToString()));
                mVelocity += force;
                break;
        }
    }

    void Jump()
    {
        if (maxJumps < 1)
        {
            doubleJumpCheck = false;
            playerAnimator.SetBool("Jump", true);
            ++maxJumps;
            mVelocity.y += Mathf.Sqrt(PlayerStats.JumpHeight * 2f * gravity);
            
        }
        else
        {
            doubleJumpCheck = true;
        }

    }

    public void SpringJump(float springforce, float rotation)
    {
        mVelocity.y += Mathf.Sqrt(PlayerStats.JumpHeight * springforce * gravity);
        mVelocity.x += rotation;
    }

    public void TakeDamage(float damage)
    {
        this.playerHealth -= damage;
        // Anim Start Dmg Sequence
        playerAnimator.SetBool("TakeDamage", true);
        IsTakingDmg = true;
    }

    public void DeathAnim()
    {
        if (deathAnim == false)
        {
           
            playerAnimator.SetBool("Death", true);
            deathAnim = true;
            
        }
        else
        {
            print("Hello");
            playerAnimator.SetBool("TakeDamage", false);
            playerAnimator.SetBool("Death", false);
        }
      

    }



}