# VentureBot

VentureBot is a 2.5D single player/co-operative bullet-hell platformer where players help a defective robot through a world of grief with the intent of becoming unimpaired by faulty programming. Players will battle hordes of enemies; 